<?php
// Parent class utama
trait Hewan
{
    public $nama, $jumlahKaki, $keahlian, $darah = 50;

    public function atraksi()
    {
        echo "{$this->nama} sedang {$this->keahlian}";
    }
}
// Parent class utama
abstract class Fight
{
    use Hewan;
    public $attackPower, $defencePower;

    public function serang($hewan)
    {
        echo "{$this->nama} sedang menyerang {$hewan->nama}";

        $hewan->diserang($this);
    }
    public function diserang($hewan)
    {
        echo "<br>{$this->nama} sedang diserang {$hewan->nama}";

        $this->darah = $this->darah - ($hewan->attackPower / $this->defencePower);
    }

    protected function getInfo()
    {
        echo "<br>";
        echo "Nama : {$this->nama}";
        echo "<br>";
        echo "Jumlah Kaki : {$this->jumlahKaki}";
        echo "<br>";
        echo "Jumlah Darah : {$this->darah}";
        echo "<br>";
        echo "Keahlian : {$this->keahlian}";
        echo "<br>";
        echo "Attack Power : {$this->attackPower}";
        echo "<br>";
        echo "Defence Power : {$this->defencePower}";
        echo "<br>";

        $this->atraksi();
    }
    
    abstract public function getInfoHewan();
}

class Elang extends Fight{
    public function __construct($nama)
    {
        $this->nama = $nama;
        $this->jumlahKaki = 2;
        $this->keahlian = "terbang tinggi";
        $this->attackPower = 10;
        $this->defencePower = 5;
    }

    public function getInfoHewan()
    {
        echo "Jenis Hewan : Elang";
        $this->getInfo();
    }
}

class Harimau extends Fight {
    public function __construct($nama)
    {
        $this->nama = $nama;
        $this->jumlahKaki = 4;
        $this->keahlian = "lari cepat";
        $this->attackPower = 7;
        $this->defencePower = 8;
    }

    public function getInfoHewan()
    {
        echo "Jenis Hewan : Harimau";
        $this->getInfo();
    }
}

class Spasi{
    public static function tampilkan(){
        echo "<br>";
        echo "=================";
        echo "<br>";
    }
}

$elang = new Elang("elang");
$elang->getInfoHewan();
Spasi::tampilkan();

$harimau = new Harimau("harimau");
$harimau->getInfoHewan();
Spasi::tampilkan();

// 1
$elang->serang($harimau);
Spasi::tampilkan();
$harimau->getInfoHewan();
Spasi::tampilkan();

// 2
$harimau->serang($elang);
Spasi::tampilkan();
$elang->getInfoHewan();
Spasi::tampilkan();

?>