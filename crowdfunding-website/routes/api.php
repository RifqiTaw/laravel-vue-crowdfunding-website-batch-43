<?php

use Illuminate\Http\Request;
use App\Http\Middleware\isAdmin;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ArticleController;
use App\Http\Controllers\CampaignController;
use App\Http\Controllers\Auth\AuthController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::group(['prefix' => 'auth',], function() {
    Route::post('register', [AuthController::class, 'register']);
    Route::post('login', [AuthController::class, 'login']);
    Route::post('generate-otp-code', [AuthController::class, 'generateOTP']);
    Route::post('verification-email', [AuthController::class, 'verifikasi']);
    Route::post('logout', [AuthController::class, 'logout'])->middleware('auth', 'email_verified');
    Route::post('update-password', [AuthController::class, 'updatePassword'])->middleware('auth', 'email_verified');
});

Route::get('get-profile', [AuthController::class, 'profile'])->middleware('auth');
Route::post('update-profile', [AuthController::class, 'updateProfile'])->middleware('auth');

Route::get('test', function() {
    return "berhasil";
})->middleware("isAdmin", "auth");

Route::apiResource('article', ArticleController::class)->middleware('auth', 'isAdmin'); 
Route::group([
    'middleware' => ['api']
], function($router){
    Route::apiResource('campaign', CampaignController::class);
});

Route::get('/getallcampaign', [CampaignController::class, 'getAll']);