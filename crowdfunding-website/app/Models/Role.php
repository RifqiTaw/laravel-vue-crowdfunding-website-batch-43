<?php

namespace App\Models;

use App\Models\User;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use App\Traits\UsesUuid;

class Role extends Model
{
    use HasFactory, UsesUuid;

    protected $fillable = ['name'];

    // protected $keyType = 'string';
    // public  $incrementing = false;
    // protected $primaryKey = ['id'];

    public function role()
    {
        return $this->hasMany(User::class, 'role_id');
    }
}
