<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use App\Models\Role;
use App\Traits\UsesUuid;
use Laravel\Sanctum\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Carbon\Carbon;
use League\CommonMark\Extension\CommonMark\Node\Inline\Code;

class User extends Authenticatable implements JWTSubject
{
    use HasApiTokens, HasFactory, Notifiable, UsesUuid;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    // protected $fillable = [
    //     'name',
    //     'email',
    //     'password',
    //     'role_id',
    //     'photo_profile'
    // ];
    protected $guarded = ['id'];

    protected $hidden = [
        'password',
        'remember_token',
    ];

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }
    
    public function getJWTCustomClaims()
    {
        return [];
    }

    protected $primaryKey = 'id';
    
    // fungsi untuk bikin automatis data role id dari table user dari id role fungsi ke dua
    public static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            $model->role_id = $model->get_role_user();
        });

        static::created(function ($model) {
            $model->generate_otp_code();
        });
    }

    public function get_role_user()
    {
        // ambil berdasarkan name dan user. ambil satu data aja
        $role = Role::where('name', 'user')->first();
        // cuman ambil id role saja
        return $role->id;
    } 

    // fungsi untuk ambil idd dari role yang namenya user
    public function generate_otp_code()
    {
        do {
            $randomNumber = mt_rand(100000, 999999);
            $check = OtpCode::where('otp', $randomNumber)->first();
        } while ($check);

        $now = Carbon::now();

        // insertOrUpdate
        $otp_code = OtpCode::updateOrCreate(
            ['user_id' => $this->id],
            [
                'otp' => $randomNumber,
                'valid_until' => $now->addMinutes(5)
            ]
        );
    }

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    
    // public function role()
    // {
    //     return $this->belongsTo(Role::class);
    // }
    
    public function role()
    {
        return $this->belongsTo(Role::class, 'role_id');
        // video terakhir gunakan user
        // return $this->belongsTo(User::class, 'role_id');
    }

    public function otpCode()
    {
        return $this->hasOne(OtpCode::class, 'user_id');
    }

    // public function isAdmin()
    // {
    //     if($this->role) {
    //         if($this->role->name === 'admin') {
    //             return true;
    //         }
    //     }
    // }

    public function isAdmin()
    {
        $role = Role::where('name', 'admin')->first();
        $idAdmin = $role->id;
        if($idAdmin) {
            return true;
        }
    } 
}
