<?php

namespace App\Http\Controllers\Auth;

use App\Models\User;
use App\Models\OtpCode;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;
use App\Events\userRegisteredEvent;
use Illuminate\Support\Facades\Auth as AuthUser;

class AuthController extends Controller
{
    // Register Akun
    public function register(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|confirmed|min:8'
        ]);

        $user = User::create([
            'name' => $request['name'],
            'email' => $request['email'],
            'password' => Hash::make($request->password),
        ]);

        // $token = Auth::login($user);
        $data['user'] = $user;

        $token = AuthUser::login($user);

        // testing
        event(new userRegisteredEvent($user));

        return response()->json([
            'response_code' => '00',
            'response_message' => 'user berhasil di register',
            'data' => $data,
            'token' => $token
        ],201);
    }

    // Login
    public function login(Request $request)
    {
        $request->validate([
            'email' => 'required|email',
            'password' => 'required',
        ]);

        $credentials = request(['email', 'password']);

        if (!$token = auth()->attempt($credentials)) {
            return response()->json(['error' => 'Email dan Password Invalid'], 401);
        }

        $data['token'] = $token;

        return response()->json([
            'response_code' => '00',
            'response_message' => 'user berhasil login',
            'data' => [
                'token' => $token
            ]
        ],201);
    }
    
    // Update Password
    public function updatePassword(Request $request)
    {
        $this->validate($request, [
            'email' => 'email|required',
            'password' => 'required|confirmed|min:6',
        ]);

        User::where('email', $request->email)->update([
            'password' => Hash::make($request->password)
        ]);

        return response()->json([
            'response_code' => '00',
            'response_message' => 'Password berhasil diubah',
        ], 200);
    }

    // Logout
    public function logout()
    {
        auth()->logout();

        return response()->json(['message' => 'Logout Berhasil']);
    }

    // Get-Profile / Get The Authenticated User
    public function profile()
    {
        $data['user'] = Auth()->user()->load('role');

        return response()->json([
            'response_code' => '00',
            'response_message' => 'Profile berhasil ditampilkan',
            'data' => $data
        ]);
    }
    
    //update profile / Update User
    public function updateProfile(Request $request)
    {
        $user = auth()->user();

        if($request->hasFile('photo_profile')) {

            $photo_profile = $request->file('photo_profile');
            $photo_profile_extension = $photo_profile->getClientOriginalExtension();
            $photo_profile_name = Str::slug($user->name, '-'). '-' .$user->id.".".$photo_profile_extension;
            $photo_profile_folder = '/photo/users/photo-profile/';
            $photo_profile_location = $photo_profile_folder . $photo_profile_name;
            try {
                $photo_profile->move(public_path($photo_profile_folder), $photo_profile_name);
                
                $user->update([
                    'photo_profile' => $photo_profile_location,
                ]);
            } catch (\Throwable $th) {
                return response()->json([
                    'response_code' => '01',
                    'response_message' => 'Profile gagal upload',
                    // 'data' => $data
                ], 200);
            }
        }

        $user->update([
            'name' => $request->name,
        ]);
        
        $data['user'] = $user;

        return response()->json([
            'response_code' => '00',
            'response_message' => 'Profile berhasil diupdate',
            'data' => $data
        ], 200);
    }

    public function generateOTP(Request $request)
    {
        $request->validate([
            'email' => 'email|required',
        ]);

        $user = User::where('email', $request->email)->first();

        $user->generate_otp_code();

        $data['user']=$user;

        // Success/True
        return response()->json([
            'response_code' => '00',
            'response_message' => 'OTP Code Berhasil di generate',
            'data' => $data
        ], 200);
    }

    public function verifikasi(Request $request)
    {
        $request->validate([
            'otp' => 'required',
        ]);

        $otp_code = OtpCode::where('otp', $request->otp)->first();

        // jika otp tidak ditemukan
        if(!$otp_code){
            return response()->json([
                'response_code' => '01',
                'response_message' => 'OTP Code tidak ditemukan'
            ], 400);
        }

        $now = Carbon::now();

        // jika otp code sudah kadaluarsa
        if($now > $otp_code->valid_until){
            return response()->json([
                'response_code' => '01',
                'response_message' => 'otp code sudah tidak berlaku, silahkan generate ulang'
            ], 400);
        }

        // update user
        $user = User::find($otp_code->user_id);
        $user->email_verified_at = $now;
        $user->save();

        //delete OTP Code
        $otp_code->delete();

        // jika response berhasil
        return response()->json([
            'response_code' => '00',
            'response_message' => 'email sudah terverifikasi',
        ], 200);
    }
}
