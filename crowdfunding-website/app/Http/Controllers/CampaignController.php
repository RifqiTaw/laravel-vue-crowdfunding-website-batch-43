<?php

namespace App\Http\Controllers;

use App\Models\Campaign;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;

class CampaignController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function __construct()
    {
        $this->middleware(['isAdmin', 'auth'])->except(['show', 'getAll']);
    }

    public function index()
    {
        $data['campaign'] = Campaign::all();

        if(count($data['campaign']) === 0){
            return response()->json([
                'response_code' => '01',
                'response_message' => 'data campaign masih kosong',
            ],200);
        }else{
            return response()->json([
                'response_code' => '01',
                'response_message' => 'tampil data berhasil',
                'data' => $data
            ],200);
        }
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'description' => 'required',
            'image' => 'mimes:jpg,png,jpeg',
            'address' => 'required',
            'required' => 'required',
            'collected' => 'required',
        ]);

        $campaign = new Campaign;

        $campaign->title = $request->title;
        $campaign->description = $request->description;
        $campaign->address = $request->address;
        $campaign->required = $request->required;
        $campaign->collected = $request->collected;

        if($request->hasFile('image')) {
            $image = $request->file('image');
            // format file
            $image_extension = $image->getClientOriginalExtension();
            //name unique file
            $image_name = time(). '.'. $image_extension;
            //lokasi penyimpanan gambar
            $image_folder = '/photo/campaign/';
            $image_location = $image_folder . $image_name;

            try {
                $image->move(public_path($image_folder), $image_name);

                $campaign->image = $image_location;
            } catch (\Throwable $th) {
                return response()->json([
                    'response_code' => '01',
                    'response_message' => 'Image gagal di upload',
                ],400);
            }
        }
        $campaign->save();

        return response()->json([
            'response_code' => '00',
            'response_message' => 'Tambah Campaign berhasil',
        ],200);
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $data = Campaign::findOrFail($id);

        return response()->json([
            'response_code' => '00',
            'response_message' => 'Detail Data Campaign',
            'data' => $data
        ],200);

    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $request->validate([
            'title' => 'required',
            'description' => 'required',
            'image' => 'mimes:jpg,png,jpeg',
            'address' => 'required',
            'required' => 'required',
            'collected' => 'required',
        ]);

        $campaign = Campaign::find($id);

        $campaign->title = $request->title;
        $campaign->description = $request->description;
        $campaign->address = $request->address;
        $campaign->required = $request->required;
        $campaign->collected = $request->collected;

        if($request->hasFile('image')) {
            $image = $request->file('image');
            // format file
            $image_extension = $image->getClientOriginalExtension();
            //name unique file
            $image_name = time(). '.'. $image_extension;
            //lokasi penyimpanan gambar
            $image_folder = '/photo/article/';

            $subCampaign = substr($campaign->image, 1);
            File::delete($subCampaign);
            $image_location = $image_folder . $image_name;

            try {
                $image->move(public_path($image_folder), $image_name);

                $campaign->image = $image_location;
            } catch (\Throwable $th) {
                return response()->json([
                    'response_code' => '01',
                    'response_message' => 'Image gagal di upload',
                ],400);
            }
        }
        $campaign->save();

        return response()->json([
            'response_code' => '00',
            'response_message' => 'Update Campaign berhasil',
        ],200);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        // $campaign = Campaign::find($id);
        $campaign = Campaign::findOrFail($id);

        $image_folder = '/photo/campaign/';

        $subCampaign = substr($campaign->image, 1);

        File::delete($subCampaign);

        $campaign->delete();

        return response()->json([
            'response_code' => '00',
            'response_message' => 'berhasil Menghapus Campaign',
        ],200);        
    }

    public function getAll()
    {
        $campaigns = Campaign::paginate(2);

        $data['campaign'] = $campaigns;

        return response()->json([
            'response_code' => '00',
            'response_message' => 'Data campaign pagination berhasil ditampilkan',
            'data' => $data
        ]);        
    }
}
