<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Models\User;

class AdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {
        $user = auth()->user();
        
        if($user->isAdmin()){
            return $next($request);
        }

        return response()->json([
            'response_code' => '00',
            'response_message' => 'Hanya Admin yang memiliki akses dihalaman ini'
        ], 401);
    }
}
