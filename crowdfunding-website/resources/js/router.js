import { createRouter, createWebHistory } from "vue-router";
import { useUserStore } from "./store/user";

const Home = () => import('@/views/Home.vue')
const Campaign = () => import('@/views/Campaign.vue')
const Verikasi = () => import('@/views/Verification.vue')
const DetailCampaign = () => import('@/views/DetailCampaign.vue')

const router = createRouter({
    // 4. Provide the history implementation to use. We are using the hash history for simplicity here.
    history: createWebHistory(),
    routes: [
        {
            path: '/',
            name: 'home',
            component: Home
        },
        {
            path: '/campaign',
            name: 'campaign',
            component: Campaign,
            meta: {
                requiredAdmin: true
            }
        },
        {
            path: '/campaign/:id',
            name: 'campaign-detail',
            component: DetailCampaign,
        },

        {
            path: '/verification',
            name: 'verification',
            component: Verikasi,
            meta: {
                requiredVeri: true
            }
        },
        {
            path: '/:catchAll(.*)',
            redirect: '/'
        }
    ] // short for `routes: routes`
  })

  router.beforeEach((to, from) => {
    const auth = useUserStore()
    if (to.meta.requiredVeri) {
        if(!auth.isNotVerification) {
            alert("Anda tidak memiliki akses dihalaman ini")
            return {
                path: '/'
            }
        }
    }

    if (to.meta.requiredAdmin) {
        if(!auth.isAdmin) {
            alert("Halaman ini cuma bisa diakses oleh admin")
            return {
                path: '/'
            }
        }
    }
  })
export default router